const express = require("express");
const cors = require("cors");
const mongoose = require("mongoose");
//const authRoutes = require("./routes/auth");
const userRoutes = require("./routes/userRoutes");
const app = express();
//const socket = require("socket.io");
require("dotenv").config();

app.use(cors());
app.use(express.json());

app.use("/api/auth", userRoutes);

mongoose
  .connect(process.env.MONGO_URL, {
    useNewUrlParser: true,
    useUnifiedTopology: true,
  })
  .then(() => {
    console.log("Conexion a la BD exitosa");
  })
  .catch((err) => {
    console.log(err.message);
  });

const server = app.listen(process.env.PORT, () =>
  console.log(`Servidor inciciado en puerto ${process.env.PORT}`)
);
