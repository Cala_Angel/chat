import React, { useState, useEffect } from "react";
import styled from "styled-components";
import Robot from "../assets/robot.gif";
export default function Welcome({ currentUser }) {
    return (
        <Container>
            <img src={Robot} alt="Robot" />
            <h1>
                Bienvenido, <span>{currentUser.username}!</span>
            </h1>
            <h3>Por favor selecciona un chat para comenzar la conversación</h3>
        </Container>
    );
}

const Container = styled.div`
    display: flex;
    justify-content: center;
    align-items: center;
    flex-direction: column;
    color: white;
    img {
        height: 20rem;
    }
    span {
        color: #4e00ff;
    }
`;